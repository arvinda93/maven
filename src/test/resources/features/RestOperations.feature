 
Feature: Execute Res
	As a user
	I want to execute rest API
	so that I can get and validate the response
	
	@get
	Scenario: Execute Get API
	Given I have rest API "https://reqres.in/api/users?page=2"
	When I execute get request for API
	Then I validate the response
	
	@post
	Scenario: Execute Get API
	Given I have rest API "https://reqres.in/api/users"
	When I execute post request for API
	Then I validate the response for post
