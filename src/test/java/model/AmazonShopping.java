package model;

import java.util.List;

import org.openqa.selenium.WebDriver;

import pageobjects.Amazon;

public class AmazonShopping {
	
	WebDriver driver;
	BaseTest base = new BaseTest();
	Amazon amazon;
	
	public void launchApplication(String url){
		driver = base.getDriver();
		driver.get(url);
	}
	
	public String getPageTitle(){
		return driver.getTitle();
	}
	
	public void searchForMobile(String keyword){
		amazon = new Amazon(driver);
		amazon.enterKeyword(keyword);
		amazon.clickSearchIcon();
	}
	
	public List<String> getModelName(){
		return amazon.getModelName();
	}

}
