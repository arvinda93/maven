package model;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class BaseTest {
	static WebDriver driver;
	static boolean isWeb = true;

	@Before
	public static void setDriver(Scenario scenarioName) {
		    String scenario = scenarioName.getName();
		    if(scenario.contains("Web")){
			System.setProperty("webdriver.chrome.driver", "C:\\new_workspace\\Day3Training\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		    }
		    else{
		    	isWeb = false;
		    }
	}

	public static WebDriver getDriver() {
		return driver;
	}

	@After
	public void tearDown() {
		if(isWeb){
			driver.close();
			driver.quit();
		}
	}
}
