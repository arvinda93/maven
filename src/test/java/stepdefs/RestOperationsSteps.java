package stepdefs;

import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.testng.Assert;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RestOperationsSteps {
	
	String endPoint;
	Response res;
	
	@Given("^I have rest API \"([^\"]*)\"$")
	public void i_have_rest_API(String apiEndPoint) throws Throwable {
		endPoint = apiEndPoint;
	}

	@When("^I execute get request for API$")
	public void i_execute_get_request_for_API() throws Throwable {
		RequestSpecBuilder requestBuilder = new RequestSpecBuilder();
		requestBuilder.setBaseUri(endPoint);
		RequestSpecification requestSpec = requestBuilder.build();
		res = RestAssured.given().spec(requestSpec).get();
		System.out.println(res.getBody().asString());
	}

	@Then("^I validate the response$")
	public void i_validate_the_response() throws Throwable {
		int statusCode = res.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		
		String lastName = res.jsonPath().get("data[4].last_name");
		
		System.out.println("lastName:"+lastName);
	}
	
	@When("^I execute post request for API$")
	public void i_execute_post_request_for_API() throws Throwable {
		String payload = "{\"name\": \"morpheus\",\"job\": \"leader\"}";
		JSONParser parse = new JSONParser();
		JSONObject jObj = (JSONObject) parse.parse(payload);
		RequestSpecBuilder requestBuilder = new RequestSpecBuilder();
		requestBuilder.setBaseUri(endPoint);
		requestBuilder.setContentType(ContentType.JSON);
		requestBuilder.setBody(jObj);
		RequestSpecification requestSpec = requestBuilder.build();
		res = RestAssured.given().spec(requestSpec).post();
		System.out.println(res.getBody().asString());
	}

	@Then("^I validate the response for post$")
	public void i_validate_the_response_for_post() throws Throwable {
		int statusCode = res.getStatusCode();
		Assert.assertEquals(statusCode, 201);
	}

}
