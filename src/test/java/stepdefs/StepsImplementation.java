package stepdefs;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Assert;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.AmazonShopping;
import model.Calculator;

public class StepsImplementation {

	int num1, num2, total;
	Calculator calculator;
	AmazonShopping amazonShopping;

	@Given("^I have a calculator$")
    public void initializeCalculator() throws Throwable {
        calculator = new Calculator();
    }

    @When("^I add (-?\\d+) and (-?\\d+)$")
    public void testAdd(int num1, int num2) throws Throwable {
        total = calculator.addNumber(num1, num2);
        System.out.println("Addition of Number " + num1 + " and " + num2 + " is:" + total);
    }

    @Then("^the result should be (-?\\d+)$")
    public void validateResult(int result) throws Throwable {
        Assert.assertThat(total, Matchers.equalTo(result));
    }

	@Given("^I open \"([^\"]*)\" website$")
	public void i_open_website(String url) throws Throwable {
		amazonShopping = new AmazonShopping();
		amazonShopping.launchApplication(url);
		System.out.println("Application launched");
		Assert.assertEquals("Amazon.com.au: Shop online for Electronics, Apparel, Toys, Books, DVDs & more",
				amazonShopping.getPageTitle());
	}

	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for(String keyword) throws Throwable {
		amazonShopping.searchForMobile(keyword);
	}

	@Then("^I print all the models$")
	public void i_print_all_the_models() throws Throwable {
		System.out.println(amazonShopping.getModelName());
	}
	
	@When("^I add customer details as per data$")
	public void i_add_customer_details_as_per(DataTable table) throws Throwable {
		List<List<String>> data = table.raw();
		String firstName = data.get(1).get(0);
		String lastName = data.get(1).get(1);
		System.out.println("FirstName:"+firstName);
		System.out.println("lastName:"+lastName);
	}
}
