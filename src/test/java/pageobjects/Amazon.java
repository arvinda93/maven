package pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Amazon{
	// This is my Change
	//Change by Rahul
	// 2nd Comment
	// 3rd Change
	WebDriver driver;
	public Amazon(WebDriver driver){
		this.driver=driver;
	}
	
	By searchBox = By.id("twotabsearchtextbox");
	By searchIcon = By.id("nav-search-submit-button");
	By modelname = By.xpath("//span[@class='a-size-base-plus a-color-base a-text-normal']");
	
	public void enterKeyword(String keyword){
		driver.findElement(searchBox).sendKeys(keyword);;
	}
	
	public void clickSearchIcon(){
		driver.findElement(searchIcon).click();
	}
	
	public List<String> getModelName(){
		List<WebElement> modelListEle = driver.findElements(modelname);
		List<String> modelList = new ArrayList<String>();
		for (WebElement model : modelListEle) {
			modelList.add(model.getText());
		}
		return modelList;
	}
}
