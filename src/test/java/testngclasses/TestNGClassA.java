package testngclasses;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class TestNGClassA {
  
  @Test(priority=3)
  public void TestA() {
	  System.out.println("Executing Test A of Class A");
  }
  
  @Test(enabled=false)
  public void TestB() {
	  System.out.println("Executing Test B of Class A");
  }
  
  @Test(priority=1)
  public void TestC() {
	  System.out.println("Executing Test C of Class A");
  }
  /*@BeforeMethod
  public void beforeMethod() {
	  System.out.println("Executing beforeMethod of Class A");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("Executing afterMethod of Class A");
  }*/

  @BeforeClass
  public void beforeClass() {
	  System.out.println("Executing beforeClass of Class A");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("Executing afterClass of Class A");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("Executing beforeTest of Class A");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("Executing afterTest of Class A");
  }

}
