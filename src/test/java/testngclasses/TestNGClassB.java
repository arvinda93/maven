package testngclasses;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class TestNGClassB {
  
  @Test
  public void TestA() {
	  System.out.println("Executing Test A of Class B");
  }
  
  @Test
  public void TestB() {
	  System.out.println("Executing Test B of Class B");
  }
  /*@BeforeMethod
  public void beforeMethod() {
	  System.out.println("Executing beforeMethod of Class B");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("Executing afterMethod of Class B");
  }*/

  @BeforeClass
  public void beforeClass() {
	  System.out.println("Executing beforeClass of Class B");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("Executing afterClass of Class B");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("Executing beforeTest of Class B");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("Executing afterTest of Class B");
  }

}
