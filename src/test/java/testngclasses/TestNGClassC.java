package testngclasses;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class TestNGClassC {
  
  @Test
  public void TestA() {
	  System.out.println("Executing Test A of Class C");
  }
  
  @Test
  public void TestB() {
	  System.out.println("Executing Test B of Class C");
  }
 /* @BeforeMethod
  public void beforeMethod() {
	  System.out.println("Executing beforeMethod of Class C");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("Executing afterMethod of Class C");
  }*/

  @BeforeClass
  public void beforeClass() {
	  System.out.println("Executing beforeClass of Class C");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("Executing afterClass of Class C");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("Executing beforeTest of Class C");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("Executing afterTest of Class C");
  }

}
